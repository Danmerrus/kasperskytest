﻿namespace KasperskyTest
{
	using System.Collections.Generic;
	using System.Threading;

	/// <summary>
	/// Класс, исполняющий первое тестовое задание.
	/// </summary>
	/// <typeparam name="T"> Тип элементов в очереди.
	/// </typeparam>
	/// <remarks>
	/// 1.Надо сделать очередь с операциями push(T) и T pop(). Операции должны поддерживать
	/// обращение с разных потоков.Операция push всегда вставляет и выходит.Операция pop ждет пока
	/// не появится новый элемент. В качестве контейнера внутри можно использовать только
	/// стандартную очередь (Queue) .
	/// </remarks>
	internal class QueueTest<T>
	{
		/// <summary>
		/// Очередь, которую нужно обернуть в потокобезопасную оболочку.
		/// </summary>
		private readonly Queue<T> queue = new Queue<T>();

		/// <summary>
		/// Положить элемент в очередь.
		/// </summary>
		/// <param name="item"> Элемент. </param>
		internal void Push(T item)
		{
			lock (queue)
			{
				queue.Enqueue(item);
				Monitor.Pulse(queue);
			}
		}

		/// <summary>
		/// Взять элемент из очереди.
		/// </summary>
		/// <returns>
		/// Элемент очереди.
		/// </returns>
		internal T Pull()
		{
			lock (queue)
			{
				Monitor.Wait(queue);
				return queue.Dequeue();
			}
		}
	}
}
