﻿namespace KasperskyTest
{
	using System;
	using System.Collections.Generic;

	/// <summary>
	/// Класс, исполняющий второе задание.
	/// </summary>
	/// <remarks>
	/// 2. Есть коллекция чисел и отдельное число Х. Надо вывести все пары чисел, которые в сумме равны заданному Х.
	/// </remarks>
	internal static class SumPair
	{
		/// <summary>
		/// Нахождение всех сумм.
		/// </summary>
		/// <param name="numbers"> Массив чисел. </param>
		/// <param name="targetNumber"> Искомый результат. </param>
		/// <returns> Массив пар чисел, которые в сумме дают искомый результат. </returns>
		internal static Tuple<int, int>[] FindSums(int[] numbers, int targetNumber)
		{
			// Сортируем, чтобы массив приобрёл необходимые для оптимизации свойства:
			// 1) Позволяет использовать бинарный поиск.
			// 2) Искать пару можно последовательно и только справа от текущего элемента.
			// 3) Если numbers[i] ^ 2 > targetNumber, следует остановиться, потому как дальше суммы будут только увеличиваться.
			Array.Sort(numbers);

			var sums = new List<Tuple<int, int>>();

			for (var i = 0; i < numbers.Length; i++)
			{
				var lesserNum = numbers[i];

				var diff = targetNumber - lesserNum;

				// Прекращаем прохождение, потому как дальше проходить бессмысленно.
				// Проверка на равенство запрещает пару из самой себя, если нужна такая пара, уберите символ '='.
				if (diff <= lesserNum)
				{
					break;
				}

				// Ищем только справа от текущего элемента.
				var diffIdx = Array.BinarySearch(numbers, i, numbers.Length - i, diff);

				if (diffIdx >= 0) 
				{
					sums.Add(new Tuple<int, int>(lesserNum, diff));
				}
			}

			return sums.ToArray();
		}
	}
}
