﻿namespace KasperskyTest
{
	using System;
	using System.Collections.Generic;
	using System.Threading;

	public class Program
	{
		/// <summary>
		/// Точка входа приложения.
		/// </summary>
		/// <param name="args"> Аргументы вызова. </param>
		public static void Main(string[] args)
		{
			Console.WriteLine("Самое время потестировать! Введите номер задания для тестирования:");

			string number;

			var isFirstTask = false;

			while (true)
			{
				number = Console.ReadLine();

				if ((isFirstTask = number == "1") || number == "2")
				{
					break;
				}

				Console.WriteLine("Ну уж нет! Давайте по правилам! Задания всего 2 {1 или 2}");
			}


			if (isFirstTask)
			{
				Console.WriteLine("Отлично! Как надоест, смело жмите Ctrl + C !");
				TestFirstTask();
			}
			else
			{
				TestSecondTask();
			}
		}

		/// <summary>
		/// Метод для тестирования первого задания.
		/// </summary>
		private static void TestFirstTask()
		{
			Console.WriteLine("Введите через пробел список слов, которые следует записать в очередь до вызова Pull'ов");

			var inputLine = Console.ReadLine();

			// Разбиваем строку по пробелам.
			var words = string.IsNullOrWhiteSpace(inputLine) 
							? new string[0] 
							: inputLine.Split(' ');

			var queue = new QueueTest<string>();

			foreach (var word in words)
			{
				queue.Push(word);
			}

			int pullThreadsCount;

			Console.WriteLine("Отлично! А теперь введите количество потоков, которые будут ждать Вас в Pull (ограничимся 100):");

			var pullThreadsCountInput = Console.ReadLine();

			// Обработка ошибок ввода.
			while (!int.TryParse(pullThreadsCountInput, out pullThreadsCount) 
				|| pullThreadsCount <= 0 
				|| pullThreadsCount > 100)
			{
				Console.WriteLine("Попробуйте ещё раз: ");
				pullThreadsCountInput = Console.ReadLine();
			}

			// Создаём потоки для тестирования Pull'а
			for (int i = 0; i < pullThreadsCount; i++)
			{
				var newThread = new Thread(PullTest);
				newThread.IsBackground = true;
				newThread.Start(queue);
			}

			Console.WriteLine("Ок. А теперь вводите строки(теперь они не будут разбиваться на слова),"
							+ "\nс которыми будет вызываться Push(Не забудьте про Ctrl + C для выхода):");

			while (true)
			{
				var input = Console.ReadLine();

				queue.Push(input);
			}
		}

		/// <summary>
		/// Метод для тестирования второго задания.
		/// </summary>
		private static void TestSecondTask()
		{
			Console.WriteLine("Введите через пробел целые числа, среди которых будут искаться пары(Ввод с ошибками будет проигнорирован):");

			var inputNumbers = Console.ReadLine();

			var numbers = new List<int>();

			while (true)
			{
				foreach (var literal in inputNumbers.Split(' '))
				{
					int number;
					if (int.TryParse(literal, out number))
					{
						numbers.Add(number);
					}
				}

				if (numbers.Count > 0)
				{
					break;
				}

				Console.WriteLine("Количество верно введённых чисел равно 0!. Попробуй ещё раз!");
			}

			Console.WriteLine("А теперь введите искомую сумму:");

			int targetNumber;
			while (true)
			{
				var inputTargetNumber = Console.ReadLine();

				if (int.TryParse(inputTargetNumber, out targetNumber))
				{
					break;
				}

				Console.WriteLine("Не получилось прочитать. Давайте ещё раз:");
			}

			Console.WriteLine("Отлично! А вот и результат:");

			var pairs = SumPair.FindSums(numbers.ToArray(), targetNumber);
			PrintArray(pairs);
		}

#region  - Вспомогательные методы - 

		/// <summary>
		/// Метод для исполнения потоками тестирования Pull'а.
		/// </summary>
		/// <param name="arg"> Параметр, в котором лежит объект потокобезопасной очереди. </param>
		private static void PullTest(object arg)
		{
			var queue = arg as QueueTest<string>;

			if (queue == null)
			{
				return;
			}

			while (true)
			{
				var item = queue.Pull();

				Console.WriteLine("Поток {0} говорит: \"{1}\"", Thread.CurrentThread.ManagedThreadId, item);
			}
		}

		/// <summary>
		/// Выводит в консоль пары чисел, разделяя их " + ".
		/// </summary>
		/// <param name="pairs"> Пары чисел </param>
		private static void PrintArray(Tuple<int, int>[] pairs)
		{
			for (var i = 0; i < pairs.Length; i++)
			{
				Console.WriteLine(pairs[i].Item1 + " + " + pairs[i].Item2);
			}
		}

#endregion // - Вспомогательные методы - 

	}
}
